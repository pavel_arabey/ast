#include <sys/types.h>
#include <stdlib.h>
#include <string.h>  
#include <unistd.h> 
#include "Serial.hpp"

#define BUFFER_PORT_SIZE 128
#define COM_PORT "/dev/ttyACM0"


struct sky_coordinates
{
	float declension;
	float right_ascension;
};

class Control
{
	sky_coordinates start_coords;
	char buffer[BUFFER_PORT_SIZE];	
	
	void str_in_sky_coordinates(char *str, sky_coordinates &coords);
	
	void sky_coordinates_in_str(char *str, sky_coordinates &coords);

public:

	Control();	
		
	Serial *serial_port;
	sky_coordinates getPosition();							// return coordinates center of telescope in the sky
	void setPosition(sky_coordinates newCoordinates);		// set coordinates something object in the sky

	void declension_UP();
	void declension_DOWN();
	
	void right_ascension_RIGHT();
	void right_ascension_LEFT(); 	

	~Control();
};
