#include "Control.hpp"

Control::Control()
{
	this->serial_port = new Serial(COM_PORT);
	for(int i = 0; i < BUFFER_PORT_SIZE; i++)
		this->buffer[i] = NULL;
	start_coords.declension = 0.0;
	start_coords.right_ascension = 0.0;
}

Control::~Control()
{
	delete this->serial_port;
}

sky_coordinates Control::getPosition()
{
	fd_set rfds;
	FD_ZERO(&rfds);
    FD_SET(this->serial_port->dSerial, &rfds);
    int retval;
    timeval tv;
    tv.tv_sec = 5;
    tv.tv_usec = 0;
	sky_coordinates coords;
	this->serial_port->WriteData("01");
	printf("write 1\n");
	retval = select(1, &rfds, NULL, NULL, &tv);
	if (retval)
       this->serial_port->ReadData(this->buffer, 4);
    else
    {
        printf("Not working...\n");
	}	
	str_in_sky_coordinates(this->buffer, coords);
	return coords;
}

void Control::setPosition(sky_coordinates newCoordinates)
{
	sky_coordinates_in_str(this->buffer, newCoordinates);
	this->serial_port->WriteData("02");
	usleep(50000);
	this->serial_port->WriteData(this->buffer);
	usleep(50000);
}

void Control::declension_UP()
{
	this->serial_port->WriteData("03");
	usleep(500000);
}

void Control::declension_DOWN()
{
	this->serial_port->WriteData("04");
	usleep(500000);
}
	
void Control::right_ascension_RIGHT()
{
	this->serial_port->WriteData("05");
	usleep(500000);
}

void Control::right_ascension_LEFT()
{
	this->serial_port->WriteData("06");
	usleep(500000);
}

void Control::str_in_sky_coordinates(char *str, sky_coordinates &coords)
{
	char temp[11];
	memcpy(temp, str, 10);
	temp[10] = '\0';
	coords.declension = atof(temp);
	memcpy(temp, str + 10, 10);
	temp[10] = '\0';
	coords.right_ascension = atof(temp);		
}
	
void Control::sky_coordinates_in_str(char *str, sky_coordinates &coords)
{
	char temp[11];
	for(int i = 0; i < 11; i++)
		temp[i] = NULL;	
	sprintf(temp, "%f", coords.declension);
	strcat(str, temp);
	strcat(str, " ");	
	sprintf(temp, "%f", coords.right_ascension);
	strcat(str, temp);
}
