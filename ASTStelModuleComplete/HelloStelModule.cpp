/*
 * Copyright (C) 2007 Fabien Chereau
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Suite 500, Boston, MA  02110-1335, USA.
 */

#include "StelProjector.hpp"
#include "StelPainter.hpp"
#include "StelApp.hpp"
#include "StelCore.hpp"
#include "StelLocaleMgr.hpp"
#include "StelModuleMgr.hpp"
#include "HelloStelModule.hpp"
#include "DateTimeDialog.hpp"


StelModule* HelloStelModuleStelPluginInterface::getStelModule() const
{
	return new HelloStelModule();
}

StelPluginInfo HelloStelModuleStelPluginInterface::getPluginInfo() const
{
	StelPluginInfo info;
	info.id = "HelloStelModule";
	info.displayedName = "AST";
	info.authors = "Pavel Arabei";
	info.contact = "aps7@tut.by";
	info.description = "mini guidance system for the telescope";
	return info;
}


HelloStelModule::HelloStelModule()
{
	setObjectName("HelloStelModule");
	font.setPixelSize(25);
}

HelloStelModule::~HelloStelModule()
{
}


double HelloStelModule::getCallOrder(StelModuleActionName actionName) const
{
	if (actionName==StelModule::ActionDraw)
		return StelApp::getInstance().getModuleMgr().getModule("NebulaMgr")->getCallOrder(actionName)+10.;
	return 0;
}
	
void HelloStelModule::handleKeys(QKeyEvent *e)
{
	switch(e->key())
	{
		case Qt::Key_8:
			control->declension_UP();
			move->panView(0.0, M_PI/360);
			break;
		case Qt::Key_2:
			control->declension_DOWN();
			move->panView(0.0, -M_PI/360);
			break;
		case Qt::Key_6:
			control->right_ascension_RIGHT();
			move->panView(M_PI/360, 0.0);
			break;
		case Qt::Key_4:
			control->right_ascension_LEFT();
			move->panView(-M_PI/360, 0.0);
			break;
		case Qt::Key_1:
			move->zoomTo(0.95, 1);	
			break;
		case Qt::Key_5: 
			move->zoomTo(5.0, 1);
			break;
		case Qt::Key_3:
			move->zoomTo(30.0, 1);
			break;
	}

}
void HelloStelModule::init()
{
        system("bash ~/ast/shellScript/test.sh");
	std::ifstream file("/home/jroux/ast/shellScript/result.txt");
	std::string longitude;
	std::getline(file, longitude);
	std::string latitude;
	std::getline(file, latitude);
	std::string time;
	std::getline(file, time);
	std::string timeZone;
	std::getline(file, timeZone);
	std::string altitude;
	std::getline(file, altitude);
	QString qlongitude = QString::fromUtf8(longitude.c_str());
	QString qlatitude = QString::fromUtf8(latitude.c_str());
	QString qtime = QString::fromUtf8(time.c_str());
	QString qaltitude = QString::fromUtf8(altitude.c_str());
	QString GMT = QString::fromUtf8(timeZone.c_str());
	QDateTime date = QDateTime::fromString(time.c_str(),"yyyy-MM-dd'T'HH:mm:ss");
	int offset = 3600*GMT.toInt();
	date = date.addSecs(offset);
	StelCore *core = StelApp::getInstance().getCore();
	core->setPresetSkyTime(date);
	core->setTodayTime(date.time());
	StelLocationMgr  &locationMgr = StelApp::getInstance().getLocationMgr();
	StelLocation *location = createLocation(qlatitude, qlongitude, qaltitude);
	locationMgr.saveUserLocation(*location);
	core->setDefaultLocationID(location->getID());
	move = (StelMovementMgr*) StelApp::getInstance().getModuleMgr().getModule(QString("StelMovementMgr"), false);	
	move->setFlagEnableMouseNavigation(false);
	move->panView( M_PI/2, 0 );
	control = new Control();	
}

double HelloStelModule::equatorialAzimuthToHorizontalAzimuth(double latitude, double phi, double t)
{
	return std::asin( std::cos(latitude * M_PI / 180) * std::sin(t * M_PI / 180) / 
			std::cos(equatorialLatToHorizontalLatitude(latitude, phi, t)) );
}


double HelloStelModule::equatorialLatToHorizontalLatitude(double latitude, double phi, double t)
{
	return std::acos(std::sin(phi * M_PI / 180) * std::sin(latitude * M_PI / 180) + 
			std::cos( phi * M_PI / 180) * std::cos(latitude * M_PI / 180) * std::cos(t * M_PI / 180));
}

StelLocation*  HelloStelModule::createLocation(QString latitude, QString longitude, QString altitude)
{
	StelLocation *location = new StelLocation();
	location->name = QString("GPSLocation");
	location->country = QString("");
	location->state = QString("");
	location->planetName = QString("Earth");
	location->longitude = longitude.toFloat();
	location->latitude = latitude.toFloat();
	location->altitude = altitude.toInt();
	location->bortleScaleIndex = 8.0;
	location->landscapeKey = QString("");
	location->population = 1;
	location->role = QChar('X');
	location->isUserLocation = true;
	return location;
}

void HelloStelModule::draw(StelCore* core)
{

}

