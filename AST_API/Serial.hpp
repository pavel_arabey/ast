#include <stdio.h>
#include <stdlib.h>
#include <string.h>  
#include <unistd.h>  
#include <fcntl.h>   
#include <errno.h>   
#include <termios.h> 
#include <sys/types.h>
#include <sys/stat.h>

#define ARDUINO_WAIT_TIME 2000

class Serial
{
    private:
        
        int dSerial;		// descriptor COM-port        
        bool connected;		// state connection        
        
    public:
        
        Serial(char *portName);									// initialization COM-port       
         
        int ReadData(char *buffer, unsigned int numbReadChar);	// read data from COM-port
        bool WriteData(char *buffer);							// write data in COM-port

        bool IsConnected();										// return state connection		
		
		~Serial();		
};
