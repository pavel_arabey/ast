#include "Control.hpp"

Control::Control()
{
	this->serial_port = new Serial(COM_PORT);
}

Control::~Control()
{
	delete this->serial_port;
}

sky_coordinates Control::getPosition()
{
	char buffer[BUFFER_PORT_SIZE];
	sky_coordinates coords;
	this->serial_port->WriteData("01");
	usleep(50000);
	this->serial_port->ReadData(buffer, 10);
	str_in_sky_coordinates(buffer, &coords);
	return coords;
}

void Control::setPosition(sky_coordinates newCoordinates)
{
	
}

void Control::declension_UP()
{
	
}

void Control::declension_DOWN()
{
	
}
	
void Control::right_ascension_RIGHT()
{
	
}

void Control::right_ascension_LEFT()
{
	
}
