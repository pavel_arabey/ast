#include "Serial.h" 

Serial::Serial(char *portName)		// initialization
{    
    this->connected = false;
    
    this->dSerial = open(portName, O_RDWR | O_NOCTTY | O_NDELAY);
    
    if (this->dSerial == -1)					// check connection   
          perror("Error with open port: ");
    else       									// set parameters
    {
		struct termios options; 
		tcgetattr(this->dSerial, &options); 
	
		cfsetispeed(&options, B9600);
		cfsetospeed(&options, B9600); 
	 
		options.c_cflag &= ~PARENB; 		/*выкл проверка четности*/
		options.c_cflag &= ~CSTOPB; 		/*выкл 2-х стобит, вкл 1 стопбит*/
		options.c_cflag &= ~CSIZE; 			/*выкл битовой маски*/
		options.c_cflag |= CS8; 			/*вкл 8бит*/
		
		if (tcsetattr(this->dSerial, TCSANOW, &options) == -1)
			perror("Error with set port`s attr: ");
		else
		{
			this->connected = true;
			usleep(ARDUINO_WAIT_TIME*1000);
		}
    }

}

Serial::~Serial()
{    
    if(this->connected)		
    {
        this->connected = false;
        close(this->dSerial);
    }
}

int Serial::ReadData(char *buffer, unsigned int numbReadChar)			// read data
{    
	int bytesSend;
	bytesSend = read(this->dSerial, buffer, numbReadChar);
	if(bytesSend == -1)
	{
		perror("Error with read from port: ");
		return -1;
	}
	return bytesSend;
}


bool Serial::WriteData(char *buffer)			// write data
{
    int bytesSend;
	unsigned int countChar = strlen(buffer);
    printf("%i", countChar);
    bytesSend = write(this->dSerial, buffer, countChar);
    if(bytesSend == -1 || bytesSend == 0)
    {
        perror("Error with write in port: ");
        return false;
    }
    else
        return true;
}

bool Serial::IsConnected()
{
    return this->connected;
}
