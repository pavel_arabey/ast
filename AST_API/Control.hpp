#include "Serial.hpp"

#define BUFFER_PORT_SIZE 128
#define COM_PORT "/dev/ttyACM0"


struct sky_coordinates
{
	float declension;
	float right_ascension;
};

class Control
{
	Serial *serial_port;
	
	void str_in_sky_coordinates(char str, sky_coordinates coords);
	void sky_coordinates_in_str(char str, sky_coordinates coords);
public:

	Control();	
	
	sky_coordinates getPosition();							// return coordinates center of telescope in the sky
	void setPosition(sky_coordinates newCoordinates);		// set coordinates something object in the sky

	//Coordinates getCurrentLocation();								// return get current location of telescope
	
	void declension_UP();
	void declension_DOWN();
	
	void right_ascension_RIGHT();
	void right_ascension_LEFT(); 	

	~Control();
};
