package com.example.gps;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TimeZone;

import android.R.integer;
import android.app.Activity;
import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.text.method.DateTimeKeyListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity  implements LocationListener{

	private LocationManager webLocationManager;
	private LocationManager gpsLocationManager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		gpsLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		webLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		webLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 3000, 10, this);
		gpsLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3000, 10, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	@Override
	public void onLocationChanged(Location location) {
	    Toast.makeText(getBaseContext(), "I am hear!!!", Toast.LENGTH_LONG).show(); 
		if (location !=  null) {
		     TimeZone tz = TimeZone.getTimeZone("UTC");
		     SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		     sdFormat.setTimeZone(tz);
		     Integer direction, offset ;
		     if(location.getLongitude() >=0)
		     {
		    	direction = 1; 
		     } else {
				direction = -1;
		     }
		     offset = (int)Math.round(direction.doubleValue() * location.getLongitude() * 24 / 360);
		     String writeString = location.getLatitude() + 
		    		 "\n" + location.getLongitude() +
		    		 "\n"+ sdFormat.format(new Date(location.getTime())) +
		    		 "\n" + offset +
		    		 "\n"+ location.getAltitude();
		     TextView textView = (TextView)findViewById(R.id.textView);                   
		     textView.setText(writeString);
		     File file = new File("/storage/emulated/0/result.txt");
			 try {
				FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
			    BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			    bufferedWriter.write(writeString);
			    bufferedWriter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}                   


	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub
		Toast.makeText(getBaseContext(), "Gps turned off ", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
		Toast.makeText(getBaseContext(), "Gps turned on ", Toast.LENGTH_LONG).show();
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub
		
	}
}
 	