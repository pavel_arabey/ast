#include <Stepper.h>
#include <TinyGPS.h>
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define BUFFER_SIZE 128
#define STEPS 100

Stepper declension_stepper(STEPS, 2, 3, 4, 5);
Stepper right_ascension_stepper(STEPS, 6, 7, 8, 9);
TinyGPS gps;
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);

char serial_buffer[BUFFER_SIZE];

class Control
{


  public:
    bool listenPort();
    bool checkCommand();
};

bool Control::listenPort()
{
  int i = 0;
  if (Serial.available())
  {
    while ( i < BUFFER_SIZE )
    {
      serial_buffer[i] = Serial.read();
      i++;
    }
    Serial.flush();
    return true;
  }
  return false;
}

bool Control::checkCommand()
{
  
}

void setup()
{
  Serial.begin(9600);
  Serial.flush();

  declension_stepper.setSpeed(30);
  right_ascension_stepper.setSpeed(30);

  lcd.begin(20, 4);
  for (int i = 0; i < 3; i++)
  {
    lcd.noBacklight();
    delay(250);
    lcd.backlight();
    delay(250);
  }
  lcd.setCursor(2, 0);
  lcd.print("AST: Start work.");
}

void loop()
{
  listenPort();
}


